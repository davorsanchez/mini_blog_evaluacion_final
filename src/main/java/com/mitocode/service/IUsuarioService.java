package com.mitocode.service;

import java.util.List;

import com.mitocode.model.Persona;
import com.mitocode.model.Usuario;

public interface IUsuarioService extends IService<Usuario> {

	Usuario login(Usuario us);
	Usuario verificarContrasena(Usuario us);
	List<Usuario> listarConsulta(Usuario us);
}
