package com.mitocode.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.mindrot.jbcrypt.BCrypt;

import com.mitocode.model.Persona;
import com.mitocode.model.Usuario;
import com.mitocode.service.IUsuarioService;

@Named
@ViewScoped
public class UsuarioBean  implements Serializable{

	private Usuario us;
	private Usuario us2;
	private Usuario usModificado;
	
	private List<Usuario> lista;
	private String activaBtn;
	private String contra;
	@Inject
	private IUsuarioService service;
	

	@PostConstruct
	public void init() {
		this.us = new Usuario();
		this.usModificado = new Usuario();
		this.us2 = new Usuario();
		this.setActivaBtn("disabled");
		this.contra = "";
	}
	
	public List<Usuario> listar() {
		this.lista = service.listarConsulta(us);
		return lista;
	}

	public void mostrarData(Usuario u) {
		this.usModificado = u;
		this.setActivaBtn("disabled");
	}
	
	public void verificarContrasena() {
		
		try {
			us2.setUsuario(this.usModificado.getUsuario());
			us2.setContrasena(this.getContra());
			Usuario usuario = service.verificarContrasena(this.us2);
			if(usuario.getUsuario() != null && usuario.getEstado().equalsIgnoreCase("A")) {
				this.us2 = usuario;
				this.setActivaBtn("enabled");
			}else {
				this.setActivaBtn("disabled");
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Aviso", "La contraseņa es incorrecta"));
			}
		}catch(Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Aviso", e.getMessage()));
		}
		
	}
	public void modificar() {
		try {
			String clave = this.us2.getContrasena();
			String claveHash = BCrypt.hashpw(clave,BCrypt.gensalt());
			this.us2.setContrasena(claveHash);
			service.modificar(this.us2);
		}catch(Exception e) {
			
		}
	}
	public void cancelar() {
		this.setActivaBtn("disabled");
		this.usModificado = new Usuario();
		this.us2 = new Usuario();
	}
	public Usuario getUs() {
		return us;
	}

	public void setUs(Usuario us) {
		this.us = us;
	}

	public Usuario getUsModificado() {
		return usModificado;
	}

	public void setUsModificado(Usuario usModificado) {
		this.usModificado = usModificado;
	}

	public List<Usuario> getLista() {
		return lista;
	}

	public void setLista(List<Usuario> lista) {
		this.lista = lista;
	}

	public String getActivaBtn() {
		return activaBtn;
	}

	public void setActivaBtn(String activaBtn) {
		this.activaBtn = activaBtn;
	}

	public Usuario getUs2() {
		return us2;
	}

	public void setUs2(Usuario us2) {
		this.us2 = us2;
	}

	public String getContra() {
		return contra;
	}

	public void setContra(String contra) {
		this.contra = contra;
	}
	
	

}
